# Datum & Docker Compose

## Getting started

1. Start the `datum-proj` service (and any others) in the background:

    ```bash
    docker-compose up -d
    ```

1. Open datum-proj at <http://localhost:8000>.

### Helpful commands

- `docker-compose exec datum-proj /bin/bash`: Get a bash shell inside your Meltano container.
- `docker-compose exec datum-proj datum-api {subcommand}`: Run a datum-api CLI Commands inside your container.
- `docker-compose logs`: See all logs.
- `docker-compose logs {service}`: See logs for a particular service, e.g. `datim-api`.

## Optional services

If these services are not relevant to you, feel free to delete their commented sections.