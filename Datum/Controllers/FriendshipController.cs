﻿using Microsoft.AspNetCore.Mvc;
using Datum.ViewModels.FriendshipModel.Request;
using Datum.ViewModels.FriendshipModel.Response;
using Datum.Services.FriendshipServices;

namespace Datum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FriendshipController : ControllerBase
    {

        private readonly IFriendshipService _friendshipService;

        public FriendshipController(IFriendshipService friendshipService)
        {
            _friendshipService = friendshipService;
        }

        // POST: api/Friendship
        [HttpPost]
        public ActionResult<FriendshipCreateResponseViewModel> FriendshipCreate(FriendshipCreateViewModel model)
        {
            var result = _friendshipService.CreateFriendship(model);

            if (!result.IsSuccessful)
                return BadRequest(result.ErrorMessage);

            var friendshipModel = result.Payload;
            return Ok(friendshipModel);
        }

        // POST: api/Friendship/5/accept
        [HttpPost("{id}/accept")]
        public ActionResult<FriendshipResponseViewModel> FriendshipAccept(int id)
        {
            var result = _friendshipService.AcceptFriendship(id);

            if (!result.IsSuccessful)
                return BadRequest(result.ErrorMessage);

            var friendshipModel = result.Payload;
            return Ok(friendshipModel);
        }

        // GET: api/Friendship/5
        [HttpGet("{id}")]
        public ActionResult<FriendshipResponseViewModel> FriendshipGetStatus(int id)
        {
            var result = _friendshipService.GetStatus(id);

            if (!result.IsSuccessful)
            {
                return BadRequest(result.ErrorMessage);
            }

            var userModel = result.Payload;
            return Ok(userModel);
        }
    }
}
