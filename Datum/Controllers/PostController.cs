﻿using Microsoft.AspNetCore.Mvc;
using Datum.ViewModels.PostModel.Request;
using Datum.ViewModels.PostModel.Response;
using Datum.Services.PostServices;

namespace Datum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {

        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        // POST: api/Post
        [HttpPost]
        public ActionResult<PostCreateResponseViewModel> PostCreate(PostCreateViewModel model)
        {
            var result = _postService.CreatePost(model);

            if (!result.IsSuccessful)
                return BadRequest(result.ErrorMessage);

            var postModel = result.Payload;
            return Ok(postModel);
        }

        // PUT: api/Post/5
        [HttpPut("{id}")]
        public ActionResult<PostGetResponseViewModel> UpdatePost(int id, [FromBody] PostUpdateViewModel model)
        {
            var result = _postService.UpdatePost(id, model);

            if (!result.IsSuccessful)
                return BadRequest(result.ErrorMessage);

            var postModel = result.Payload;
            return Ok(postModel);

        }

        // GET: api/Post/5
        [HttpGet("{id}")]
        public ActionResult<PostGetResponseViewModel> GetPost(int id)
        {
            var result = _postService.GetPost(id);

            if (!result.IsSuccessful)
                return BadRequest(result.ErrorMessage);

            var postModel = result.Payload;
            return Ok(postModel);
        }



        // DELETE: api/Post/5
            [HttpDelete("{id}")]
        public ActionResult<PostResponseViewModel> DeletePost(int id)
        {
            var result = _postService.DeletePost(id);

            if (!result.IsSuccessful)
            {
                return BadRequest(result.ErrorMessage);
            }

            var postModel = result.Payload;
            return Ok(postModel);
        }
    }
}
