﻿using Datum.Services.UserServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Datum.ViewModels.UserModel.Request;
using Datum.ViewModels.UserModel.Response;

namespace Datum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        // POST: api/Users
        [HttpPost]
        [AllowAnonymous]
        public ActionResult<UserCreateResponseViewModel> PostUser([FromBody] UserCreateViewModel model)
        {
            var result = _userService.CreateUser(model);

            if (!result.IsSuccessful)
            {
                return BadRequest(result.ErrorMessage);
            }

            var userModel = result.Payload;
            return Ok(userModel);
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public ActionResult<UserGetResponseViewModel> GetUser(string id)
        {
            var result = _userService.GetUser(id);

            if (!result.IsSuccessful)
            {
                return BadRequest(result.ErrorMessage);
            }

            var userModel = result.Payload;
            return Ok(userModel);
        }

        // GET ALL: api/Users
        [HttpGet]
        [AllowAnonymous]
        public ActionResult<UserResponseViewModel> GetUsers()
        {
            var result = _userService.GetAllUsers();

            if (!result.IsSuccessful)
            {
                return BadRequest(result.ErrorMessage);
            }

            var userModel = result.Payload;
            return Ok(userModel);
        }


        // PUT: api/Users/5
        [HttpPut("{id}")]
        public ActionResult<UserResponseViewModel> UpdateUser(string id, [FromBody] UserUpdateViewModel model)
        {
            var result = _userService.UpdateUser(id!, model);

            if (!result.IsSuccessful)
                return BadRequest(result.ErrorMessage);

            var userModel = result.Payload;
            return Ok(userModel);
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        [AllowAnonymous]
        public ActionResult<UserResponseViewModel> DeleteUser(string id)
        {
            var result = _userService.DeleteUser(id);

            if (!result.IsSuccessful)
                return BadRequest(result.ErrorMessage);

            var userModel = result.Payload;
            return Ok(userModel);
        }
    }
}
