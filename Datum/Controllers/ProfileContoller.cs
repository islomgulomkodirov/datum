﻿using Microsoft.AspNetCore.Mvc;
using Datum.ViewModels.ProfileModel.Response;
using Datum.Services.ProfileServices;
using Datum.ViewModels.ProfileModel.Request;

namespace Datum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {

        private readonly IProfileService _provProfileService;

        public ProfileController(IProfileService profileService)
        {
            _provProfileService = profileService;
        }

        // GET: api/Profiles/5
        [HttpGet("{id}")]
        public ActionResult<ProfileGetResponseViewModel> ProfileGet(int id)
        {
            var result = _provProfileService.GetProfileById(id);

            if (!result.IsSuccessful)
            {
                return BadRequest(result.ErrorMessage);
            }

            var userModel = result.Payload;
            return Ok(userModel);
        }

        // PUT: api/Profiles/5
        [HttpPut("{id}")]
        public ActionResult<ProfileResponseViewModel> ProfileUpdate(int id, [FromBody] ProfileUpdateViewModel model)
        {
            var result = _provProfileService.UpdateProfile(id, model);

            if (!result.IsSuccessful)
            {
                return BadRequest(result.ErrorMessage);
            }

            var profileModel = result.Payload;
            return Ok(profileModel);
        }
    }
}
