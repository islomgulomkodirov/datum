﻿using Microsoft.AspNetCore.Mvc;
using Datum.ViewModels.CommentModel.Request;
using Datum.ViewModels.CommentModel.Response;
using Datum.Services.CommentServices;

namespace Datum.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController: ControllerBase
    {

        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        // POST: api/Comments
        [HttpPost]
        public ActionResult<CommentCreateResponseModel> CreateComment([FromBody] CommentCreateViewModel model)
        {
            var result = _commentService.CreateComment(model);

            if (!result.IsSuccessful)
            {
                return BadRequest(result.ErrorMessage);
            }

            var commentModel = result.Payload;
            return Ok(commentModel);
        }

        // GET: api/Comments/5
        [HttpGet("{id}")]
        public ActionResult<CommentGetResponseModel> GetComment(int id)
        {
            var result = _commentService.GetComment(id);

            if (!result.IsSuccessful)
            {
                return BadRequest(result.ErrorMessage);
            }

            var commentModel = result.Payload;
            return Ok(commentModel);
        }


        // PUT: api/Comments/5
        [HttpPut("{id}")]
        public ActionResult<CommentGetResponseModel> UpdateComment(int id, [FromBody] CommentUpdateViewModel model)
        {
            var result = _commentService.UpdateComment(id!, model);

            if (!result.IsSuccessful)
                return BadRequest(result.ErrorMessage);

            var commentModel = result.Payload;
            return Ok(commentModel);
        }

        // DELETE: api/Comments/5
        [HttpDelete("{id}")]
        public ActionResult<CommentResponseModel> DeleteComment(int id)
        {
            var result = _commentService.DeleteComment(id);

            if (!result.IsSuccessful)
                return BadRequest(result.ErrorMessage);

            var commentModel = result.Payload;
            return Ok(commentModel);
        }
    }
}
