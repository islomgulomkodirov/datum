﻿using Datum.Models;
using Datum.ViewModels;
using Datum.ViewModels.CommentModel.Request;
using Datum.ViewModels.CommentModel.Response;
using Datum.Repositories.CommentRepo;
using Datum.ViewModels.PostModel.Response;

namespace Datum.Services.CommentServices
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;

        public CommentService(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }


        public ServiceResultViewModel<CommentCreateResponseModel> CreateComment(CommentCreateViewModel model)
        {
            var comment = new Comment()
            {
                PostId = model.PostId,
                UserId = model.UserId,
                Title = model.Title,
                Text = model.Text
            };

            _commentRepository.Create(comment);


            return new ServiceResultViewModel<CommentCreateResponseModel>
            {
                IsSuccessful = true,
                Payload = new CommentCreateResponseModel()
                {
                    Id = comment.Id,
                    PostId = model.PostId,
                    UserId = model.UserId,
                    Title = model.Title,
                    Text = model.Text,
                    CreatedAt = comment.CreatedAt
                }
            };
        }

        public ServiceResultViewModel<CommentGetResponseModel> UpdateComment(int id, CommentUpdateViewModel model)
        {
            var comment = _commentRepository.Get(id);

            if (comment == null)
            {
                return new ServiceResultViewModel<CommentGetResponseModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "Comment is not found"
                };
            }

            comment.Title = model.Title;
            comment.Text = model.Text;

            comment = _commentRepository.Update(comment);
            return new ServiceResultViewModel<CommentGetResponseModel>()
            {
                IsSuccessful = true,
                Payload = new CommentGetResponseModel()
                {
                    Id = comment!.Id,
                    PostId = comment.PostId,
                    UserId = comment.UserId,
                    Title = comment.Title,
                    Text = comment.Text,
                    CreatedAt = comment.CreatedAt
                }
            };
        }

        public ServiceResultViewModel<CommentGetResponseModel> GetComment(int id)
        {
            var comment = _commentRepository.Get(id);

            if (comment == null)
            {
                return new ServiceResultViewModel<CommentGetResponseModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "Comment not found"
                };
            }


            return new ServiceResultViewModel<CommentGetResponseModel>()
            {
                IsSuccessful = true,
                Payload = new CommentGetResponseModel()
                {
                    Id = comment!.Id,
                    PostId = comment.PostId,
                    UserId = comment.UserId,
                    Title = comment.Title,
                    Text = comment.Text,
                    CreatedAt = comment.CreatedAt

                }
            };
        }

        public ServiceResultViewModel<CommentResponseModel> DeleteComment(int id)
        {
            var comment = _commentRepository.Get(id!);

            if (comment == null)
                return new ServiceResultViewModel<CommentResponseModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "Comment not found"
                };

            _commentRepository.Delete(comment);

            return new ServiceResultViewModel<CommentResponseModel>()
            {
                IsSuccessful = true,
                Payload = new CommentResponseModel()
                {
                    Id = id,
                    CreatedAt = DateTime.Now
                }
            };
        }

        public ServiceResultViewModel<List<CommentGetResponseModel>> GetUserComments(string userId)
        {
            var comments = _commentRepository.GetCommentsByUserId(userId);

            if (comments == null)
            {
                return new ServiceResultViewModel<List<CommentGetResponseModel>>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "User has no comments"
                };
            }
            var commentsViewModel = comments.Select(comment => new CommentGetResponseModel()
            {
                Id = comment.Id,
                PostId = comment.PostId,
                UserId = comment.UserId,
                Title = comment.Title,
                Text = comment.Text,
                CreatedAt = comment.CreatedAt
            }).ToList();

            return new ServiceResultViewModel<List<CommentGetResponseModel>>()
            {
                IsSuccessful = true,
                Payload = commentsViewModel
            };
        }

        public ServiceResultViewModel<List<CommentGetResponseModel>> GetPostComments(int postId)
        {
            var comments = _commentRepository.GetCommentsByPostId(postId);

            if (comments == null)
            {
                return new ServiceResultViewModel<List<CommentGetResponseModel>>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "User has no comments"
                };
            }
            var commentsViewModel = comments.Select(comment => new CommentGetResponseModel()
            {
                Id = comment.Id,
                PostId = comment.PostId,
                UserId = comment.UserId,
                Title = comment.Title,
                Text = comment.Text,
                CreatedAt = comment.CreatedAt
            }).ToList();

            return new ServiceResultViewModel<List<CommentGetResponseModel>>()
            {
                IsSuccessful = true,
                Payload = commentsViewModel
            };
        }
    }
}