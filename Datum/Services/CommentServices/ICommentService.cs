﻿using Datum.ViewModels;
using Datum.ViewModels.CommentModel.Request;
using Datum.ViewModels.CommentModel.Response;

namespace Datum.Services.CommentServices
{
    public interface ICommentService
    {

        ServiceResultViewModel<CommentCreateResponseModel> CreateComment(CommentCreateViewModel model);

        ServiceResultViewModel<CommentGetResponseModel> UpdateComment(int id, CommentUpdateViewModel model);

        ServiceResultViewModel<CommentGetResponseModel> GetComment(int id);

        ServiceResultViewModel<CommentResponseModel> DeleteComment(int id);

        ServiceResultViewModel<List<CommentGetResponseModel>> GetUserComments(string userId);

        ServiceResultViewModel<List<CommentGetResponseModel>> GetPostComments(int postId);



    }
}
