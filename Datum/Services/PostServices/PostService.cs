﻿using Datum.Models;
using Datum.ViewModels;
using Datum.ViewModels.PostModel.Request;
using Datum.ViewModels.PostModel.Response;
using Datum.Repositories.PostRepo;

namespace Datum.Services.PostServices
{
    public class PostService : IPostService
    {
        private readonly IPostRepository _postRepository;

        public PostService(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        public ServiceResultViewModel<PostCreateResponseViewModel> CreatePost(PostCreateViewModel model)
        {

            var post = new Post()
            {
                Name = model.Name!,
                Description = model.Description!,
                UserId = model.UserId,
            };

            var postResponse = _postRepository.Create(post);

            if (postResponse == null)
            {
                return new ServiceResultViewModel<PostCreateResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "Could not create a post"
                };
            }


            return new ServiceResultViewModel<PostCreateResponseViewModel>()
            {
                IsSuccessful = true,
                Payload = new PostCreateResponseViewModel()
                {
                    Id = postResponse.Id!,
                    UserId = postResponse.UserId,
                    Name = postResponse.Name
                }
            };
        }

        public ServiceResultViewModel<PostGetResponseViewModel> UpdatePost(int id, PostUpdateViewModel model)
        {
            var post = _postRepository.Get(id);

            if (post == null)
            {
                return new ServiceResultViewModel<PostGetResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "Post is not found"
                };
            }

            post.Name = model.Name;
            post.Description = model.Description;

            post = _postRepository.Update(post);
            return new ServiceResultViewModel<PostGetResponseViewModel>()
            {
                IsSuccessful = true,
                Payload = new PostGetResponseViewModel()
                {
                    Id = post!.Id,
                    Name = post.Name,
                    Description = post.Description,
                    User = post.User,
                    Comments = post.Comments,
                    CreatedAt = post.CreatedAt
                }
            };
        }

        public ServiceResultViewModel<PostGetResponseViewModel> GetPost(int id)
        {
            var post = _postRepository.Get(id);

            if (post == null)
            {
                return new ServiceResultViewModel<PostGetResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "Post not found"
                };
            }

            return new ServiceResultViewModel<PostGetResponseViewModel>()
            {
                IsSuccessful = true,
                Payload = new PostGetResponseViewModel()
                {
                    Id = post!.Id,
                    Name = post.Name,
                    Description = post.Description,
                    User = post.User,
                    Comments = post.Comments,
                    CreatedAt = post.CreatedAt
                }
            };
        }

        public ServiceResultViewModel<PostResponseViewModel> DeletePost(int id)
        {
            var post = _postRepository.Get(id);


            if (post == null)
            {
                return new ServiceResultViewModel<PostResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "Post not found"
                };
            }

            var createdAt = post.CreatedAt;


            _postRepository.Delete(post.Id);

            return new ServiceResultViewModel<PostResponseViewModel>()
            {
                IsSuccessful = true,
                Payload = new PostResponseViewModel()
                {
                    Id = id,
                    CreatedAt = createdAt
                }
            };
        }

        public ServiceResultViewModel<List<PostResponseViewModel>> GetPostsByUserId(string id)
        {
            var posts = _postRepository.GetUserPosts(id);

            if (posts == null)
            {
                return new ServiceResultViewModel<List<PostResponseViewModel>>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "User has no posts"
                };
            }
            var postsViewModel = posts.Select(post => new PostResponseViewModel()
            {
                Id = post.Id, CreatedAt = post.CreatedAt
            }).ToList();

            return new ServiceResultViewModel<List<PostResponseViewModel>>()
            {
                IsSuccessful = true,
                Payload = postsViewModel
            };
        }
    }
}
