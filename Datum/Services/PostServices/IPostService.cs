﻿using Datum.ViewModels;
using Datum.ViewModels.PostModel.Request;
using Datum.ViewModels.PostModel.Response;

namespace Datum.Services.PostServices
{
    public interface IPostService
    {

        ServiceResultViewModel<PostCreateResponseViewModel> CreatePost(PostCreateViewModel model);

        ServiceResultViewModel<PostGetResponseViewModel> UpdatePost(int id, PostUpdateViewModel model);

        ServiceResultViewModel<PostGetResponseViewModel> GetPost(int id);

        ServiceResultViewModel<PostResponseViewModel> DeletePost(int id);

        ServiceResultViewModel<List<PostResponseViewModel>> GetPostsByUserId(string id);

    }
}
