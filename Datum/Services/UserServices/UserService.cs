﻿using Datum.Models;
using Datum.Repositories.ProfileRepo;
using Datum.Repositories.UserRepo;
using Datum.ViewModels;
using Datum.ViewModels.ProfileModel.Response;
using Datum.ViewModels.UserModel.Request;
using Datum.ViewModels.UserModel.Response;
using Microsoft.AspNetCore.Mvc;

namespace Datum.Services.UserServices
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IProfileRepository _profileRepository;

        public UserService(IUserRepository userRepository, IProfileRepository profileRepository)
        {
            _userRepository = userRepository;
            _profileRepository = profileRepository;
        }

        public ServiceResultViewModel<List<UserResponseViewModel>> GetAllUsers()
        {

            var users = _userRepository.GetAll();

            if (users == null || !users.Any())
            {
                return new ServiceResultViewModel<List<UserResponseViewModel>>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "No users are found"

                };
            }

            var usersViewModel = users.Select(user => new UserResponseViewModel()
            { Id = user.Id, UserName = user.UserName, UserEmail = user.Email }).ToList();

            return new ServiceResultViewModel<List<UserResponseViewModel>>()
            {
                IsSuccessful = true,
                Payload = usersViewModel

            };
        }

        public ServiceResultViewModel<UserCreateResponseViewModel> CreateUser(UserCreateViewModel model)
        {
            if (_userRepository.CheckUserExistsByUsername(model.UserName!))
            {
                return new ServiceResultViewModel<UserCreateResponseViewModel>
                {
                    IsSuccessful = false,
                    ErrorMessage = "User with a given username is already exists"
                };
            }

            var user = new User()
            {
                UserName = model.UserName,
                Email = model.Email,
                PasswordHash = model.Password
            };

            _userRepository.Create(user);

            if (!_userRepository.CheckUserHasProfile(user))
            {
                var profile = new Profile()
                {
                    Id = new Random().Next(10000),
                    FullName = model.UserName,
                    Bio = "Hey, it's " + model.UserName + ". I am just a regular user.",
                    User = user,
                    UserId = user.Id
                };

                _profileRepository.Create(profile);
            }

            return new ServiceResultViewModel<UserCreateResponseViewModel>
            {
                IsSuccessful = true,
                Payload = new UserCreateResponseViewModel()
                {
                    Id = user.Id,
                    UserName = model.UserName,
                    UserEmail = model.Email,
                    ProfileId = user.Profile!.Id
                }
            };
        }

        public ServiceResultViewModel<UserResponseViewModel> UpdateUser(string id, [FromBody] UserUpdateViewModel model)
        {
            var user = _userRepository.Get(id);

            if (user == null)
            {
                return new ServiceResultViewModel<UserResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "User is not found"
                };
            }

            user.UserName = model.UserName;
            user.Email = model.UserEmail;

            if (!string.IsNullOrEmpty(model.Password))
            {
                var passwordHash = _userRepository.HashPassword(model.Password);

                user.PasswordHash = passwordHash;
            }

            user = _userRepository.Update(user);

            return new ServiceResultViewModel<UserResponseViewModel>()
            {
                IsSuccessful = true,
                Payload = new UserResponseViewModel()
                {
                    Id = user!.Id,
                    UserName = user.UserName,
                    UserEmail = user.Email,
                }
            };
        }

        public ServiceResultViewModel<UserGetResponseViewModel> GetUser(string id)
        {
            var user = _userRepository.Get(id);

            if (user == null)
            {
                return new ServiceResultViewModel<UserGetResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "User not found"
                };
            }

            var profile = _profileRepository.GetByUser(user);

            var profileViewModel = new ProfileGetResponseViewModel();

            if (profile != null)
            {
                profileViewModel = new ProfileGetResponseViewModel()
                {
                    Id = profile.Id,
                    FullName = profile.FullName,
                    Bio = profile.Bio,
                    AvatarUrl = profile.AvatarUrl
                };
            }


            return new ServiceResultViewModel<UserGetResponseViewModel>()
            {
                IsSuccessful = true,
                Payload = new UserGetResponseViewModel()
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    UserEmail = user.Email,
                    Profile = profileViewModel,
                    Friends = user.Friendships.ToList(),
                    Posts = user.Posts.ToList(),
                    Comments = user.Comments.ToList()

                }
            };
        }

        public ServiceResultViewModel<UserResponseViewModel> DeleteUser(string id)
        {
            var user = _userRepository.Get(id!);

            if (user == null)
                return new ServiceResultViewModel<UserResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "User not found"
                };

            _userRepository.Delete(user);

            return new ServiceResultViewModel<UserResponseViewModel>()
            {
                IsSuccessful = true,
                Payload = new UserResponseViewModel()
                {
                    Id = id,
                    UserName = "Deleted",
                    UserEmail = "Deleted"
                }
            };
        }
    }
}
