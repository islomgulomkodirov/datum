﻿using Datum.ViewModels;
using Datum.ViewModels.UserModel.Request;
using Datum.ViewModels.UserModel.Response;

namespace Datum.Services.UserServices
{
    public interface IUserService
    {
        ServiceResultViewModel<List<UserResponseViewModel>> GetAllUsers();

        ServiceResultViewModel<UserCreateResponseViewModel> CreateUser(UserCreateViewModel model);

        ServiceResultViewModel<UserResponseViewModel> UpdateUser(string id, UserUpdateViewModel model);

        ServiceResultViewModel<UserGetResponseViewModel> GetUser(string id);

        ServiceResultViewModel<UserResponseViewModel> DeleteUser(string id);
    }
}
