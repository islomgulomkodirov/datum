﻿using Datum.ViewModels;
using Datum.ViewModels.ProfileModel.Request;
using Datum.ViewModels.ProfileModel.Response;

namespace Datum.Services.ProfileServices
{
    public interface IProfileService
    {

        ServiceResultViewModel<ProfileResponseViewModel> UpdateProfile(int id, ProfileUpdateViewModel model);

        ServiceResultViewModel<ProfileGetResponseViewModel> GetProfileById(int id);

    }
}
