﻿using Datum.ViewModels;
using Datum.ViewModels.ProfileModel.Request;
using Datum.ViewModels.ProfileModel.Response;
using Datum.Repositories.ProfileRepo;
using Microsoft.AspNetCore.Mvc;

namespace Datum.Services.ProfileServices
{
    public class ProfileService : IProfileService
    {
        private readonly IProfileRepository _profileRepository;

        public ProfileService(IProfileRepository profileRepository)
        {
            _profileRepository = profileRepository;
        }


        public ServiceResultViewModel<ProfileResponseViewModel> UpdateProfile(int id, ProfileUpdateViewModel model)
        {
            var profile = _profileRepository.Get(id);

            if (profile == null)
            {
                return new ServiceResultViewModel<ProfileResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "Profile is not found"
                };
            }

            profile.FullName = model.FullName;
            profile.Bio = model.Bio;
            profile.AvatarUrl = model.AvatarUrl;

            profile = _profileRepository.Update(profile);
            return new ServiceResultViewModel<ProfileResponseViewModel>()
            {
                IsSuccessful = true,
                Payload = new ProfileResponseViewModel()
                {
                    Id = profile!.Id,
                    FullName = profile.FullName,
                    Bio = profile.Bio,
                    AvatarUrl = profile.AvatarUrl
                }
            };
        }

        public ServiceResultViewModel<ProfileGetResponseViewModel> GetProfileById(int id)
        {
            var profile = _profileRepository.Get(id);

            if (profile == null)
            {
                return new ServiceResultViewModel<ProfileGetResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "User not found"
                };
            }


            return new ServiceResultViewModel<ProfileGetResponseViewModel>()
            {
                IsSuccessful = true,
                Payload = new ProfileGetResponseViewModel()
                {
                    Id = profile.Id,
                    FullName = profile.FullName,
                    Bio = profile.Bio,
                    AvatarUrl = profile.AvatarUrl,
                    User = profile.User
                }
            };
        }
    }
}
