﻿using Datum.ViewModels;
using Datum.ViewModels.FriendshipModel.Request;
using Datum.ViewModels.FriendshipModel.Response;

namespace Datum.Services.FriendshipServices
{
    public interface IFriendshipService
    {

        ServiceResultViewModel<FriendshipCreateResponseViewModel> CreateFriendship(FriendshipCreateViewModel model);

        ServiceResultViewModel<FriendshipResponseViewModel> AcceptFriendship(int id);

        ServiceResultViewModel<FriendshipResponseViewModel> GetStatus(int id);

    }
}
