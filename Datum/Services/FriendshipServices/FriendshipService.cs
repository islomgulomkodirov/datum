﻿using Datum.Models;
using Datum.ViewModels;
using Datum.ViewModels.FriendshipModel.Request;
using Datum.ViewModels.FriendshipModel.Response;
using Datum.Repositories.UserRepo;
using Datum.Repositories.FriendshipRepo;

namespace Datum.Services.FriendshipServices
{
    public class FriendshipService : IFriendshipService
    {
        private readonly IFriendshipRepository _friendshipRepository;
        private readonly IUserRepository _userRepository;

        public FriendshipService(IFriendshipRepository friendshipRepository, IUserRepository userRepository)
        {
            _friendshipRepository = friendshipRepository;
            _userRepository = userRepository;
        }

        public ServiceResultViewModel<FriendshipCreateResponseViewModel> CreateFriendship(FriendshipCreateViewModel model)
        {
            var userA = _userRepository.Get(model.User1Id!);
            var userB = _userRepository.Get(model.User2Id!);

            if (userA == null || userB == null)
            {
                return new ServiceResultViewModel<FriendshipCreateResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "User is not found"
                };
            }

            if (_friendshipRepository.FriendshipExists(model.User1Id!, model.User2Id!))
            {
                return new ServiceResultViewModel<FriendshipCreateResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "Friendship between these users already exists"
                };
            }

            var friendship = new Friendship()
            {
                IsAccepted = false,
                Users = new List<User>() { userA, userB }
            };

            var friendshipResponse = _friendshipRepository.Create(friendship);

            if (friendshipResponse == null)
            {
                return new ServiceResultViewModel<FriendshipCreateResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "Friendship is not established, something went wrong"
                };
            }


            return new ServiceResultViewModel<FriendshipCreateResponseViewModel>()
            {
                IsSuccessful = true,
                Payload = new FriendshipCreateResponseViewModel()
                {
                    Id = friendship.Id
                }
            };
        }

        public ServiceResultViewModel<FriendshipResponseViewModel> AcceptFriendship(int id)
        {
            var friendship = _friendshipRepository.Accept(id);

            if (friendship == null)
            {
                return new ServiceResultViewModel<FriendshipResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "Friendship is not accepted"
                };
            }


            return new ServiceResultViewModel<FriendshipResponseViewModel>()
            {
                IsSuccessful = true,
                Payload = new FriendshipResponseViewModel()
                {
                    Id = friendship.Id
                }
            };
        }

        public ServiceResultViewModel<FriendshipResponseViewModel> GetStatus(int id)
        {
            var friendship = _friendshipRepository.Get(id);

            if (friendship == null)
            {
                return new ServiceResultViewModel<FriendshipResponseViewModel>()
                {
                    IsSuccessful = false,
                    ErrorMessage = "Friendship not found"
                };
            }


            return new ServiceResultViewModel<FriendshipResponseViewModel>()
            {
                IsSuccessful = true,
                Payload = new FriendshipResponseViewModel()
                {
                    Id = friendship.Id,
                    IsAccepted = friendship.IsAccepted
                }
            };
        }
    }
}
