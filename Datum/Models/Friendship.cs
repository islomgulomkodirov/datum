﻿using System.ComponentModel.DataAnnotations;

namespace Datum.Models
{
    public class Friendship
    {
        [Key] public int Id { get; set; }
        public bool IsAccepted { get; set; }

        public ICollection<User>? Users { get; set; } = new List<User>();

        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
