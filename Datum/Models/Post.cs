﻿using System.ComponentModel.DataAnnotations;

namespace Datum.Models
{
    public class Post
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; } = string.Empty;

        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; } = string.Empty;

        [Required(ErrorMessage = "User Id is required")] 
        public string? UserId { get; set; }

        public User User { get; set; } = null!;

        public List<Comment>? Comments { get; set; }

        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
