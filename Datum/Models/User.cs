﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Datum.Models
{
    public class User : IdentityUser
    {
        [Key] public override string Id { get; set; } = Guid.NewGuid().ToString();

        public Profile? Profile { get; set; }
        public ICollection<Friendship> Friendships { get; set; } = new List<Friendship>();

        public ICollection<Post> Posts { get; set; } = new List<Post>();

        public ICollection<Comment> Comments { get; set; } = new List<Comment>();
    }
}