﻿using System.ComponentModel.DataAnnotations;

namespace Datum.Models
{
    public class Profile
    {
        [Key]
        public int Id { get; set; }
        public string? FullName { get; set; }

        public string? Bio { get; set; }

        public string? AvatarUrl { get; set; }

        [Required(ErrorMessage = "User Id is required")]
        public string?  UserId { get; set; }

        public User User { get; set; } = null!;


    }
}
