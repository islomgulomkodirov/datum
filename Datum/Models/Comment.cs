﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Datum.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Post Id is required")]
        public int PostId { get; set; }
        public Post? Post { get; set; } = null!;

        [Required(ErrorMessage = "User Id is required")]
        public string? UserId { get; set; }
        public User? User { get; set; } = null!;

        [Required(ErrorMessage = "Title is required")]
        public string? Title { get; set; } = string.Empty;
        public string? Text { get; set; } = string.Empty;

        public DateTime CreatedAt { get; set; } = DateTime.Now;

    }
}
