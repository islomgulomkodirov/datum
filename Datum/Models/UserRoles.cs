﻿using Datum.Models;

namespace Datum.Models
{
    public class UserRoles
    {
        public static string Admin = "Admin";
        public static string User = "User";
    }
}
