﻿using Datum.Data;
using Datum.Models;

namespace Datum.Repositories.PostRepo
{
    public class PostRepository : IPostRepository
    {
        private readonly DataContext _dataContext;

        public PostRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }


        public Post? Create(Post post)
        {
            _dataContext.Posts.Add(post);
            _dataContext.SaveChanges();

            return post;
        }

        public Post? Update(Post post)
        {
            _dataContext.Posts.Update(post);
            _dataContext.SaveChanges();

            return post;
        }

        public Post? Get(int id)
        {
            return _dataContext.Posts.Find(id);
        }

        public void Delete(int id)
        {
            var post = _dataContext.Posts.Find(id);

            if (post != null)
                _dataContext.Posts.Remove(post);
        }

        public List<Post> GetUserPosts(string userId)
        {
            return _dataContext.Posts.Where(p => p.UserId == userId).ToList();
        }
    };
}
