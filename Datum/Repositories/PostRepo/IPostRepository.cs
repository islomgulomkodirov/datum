﻿using Datum.Models;

namespace Datum.Repositories.PostRepo
{
    public interface IPostRepository
    {
        Post? Create(Post post);

        Post? Update(Post post);

        Post? Get(int id);

        void Delete(int id);

        List<Post>? GetUserPosts (string userId);
    }
}
