﻿using Datum.Data;
using Datum.Models;

namespace Datum.Repositories.FriendshipRepo
{
    public class FriendshipRepository : IFriendshipRepository
    {
        private readonly DataContext _dataContext;

        public FriendshipRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public Friendship? Create(Friendship friendship)
        {
            _dataContext.Friendships.Add(friendship);
            _dataContext.SaveChanges();

            return friendship;
        }

        public Friendship? Accept(int id)
        {
            var friendship = _dataContext.Friendships.FirstOrDefault(x => x.Id == id);

            if (friendship == null)
            {
                return null;
            }

            using var transaction = _dataContext.Database.BeginTransaction();
            try
            {
                friendship.IsAccepted = true;
                _dataContext.SaveChanges();
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new Exception("Error accepting friendship", ex);
            }

            return friendship;
        }

        public Friendship? Get(int id)
        {
            return _dataContext.Friendships.Find(id);
        }

        public bool FriendshipExists(string userId1, string userId2)
        {
            return _dataContext.Friendships.Any(f => f.Users!.Any(u => u.Id == userId1)
                                                     && f.Users!.Any(u => u.Id == userId2));
        }
    };
}
