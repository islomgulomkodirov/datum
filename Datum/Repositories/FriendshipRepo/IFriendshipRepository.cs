﻿using Datum.Models;

namespace Datum.Repositories.FriendshipRepo
{
    public interface IFriendshipRepository
    {
        Friendship? Create(Friendship friendship);

        Friendship? Accept(int id);

        Friendship? Get(int id);

        bool FriendshipExists(string userId1, string userId2);
    }
}
