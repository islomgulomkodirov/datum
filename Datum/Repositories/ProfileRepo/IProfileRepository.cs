﻿using Datum.Models;

namespace Datum.Repositories.ProfileRepo
{
    public interface IProfileRepository
    {
        Profile? Create(Profile profile);

        Profile? Update(Profile profile);

        Profile? Get(int id);

        Profile? GetByUser(User user);
    }
}
