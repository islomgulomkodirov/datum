﻿using Datum.Data;
using Datum.Models;

namespace Datum.Repositories.ProfileRepo
{
    public class ProfileRepository : IProfileRepository
    {
        private readonly DataContext _dataContext;

        public ProfileRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public Profile? Create(Profile profile)
        {
            _dataContext.Profiles.Add(profile);
            _dataContext.SaveChanges();

            return profile;
        }

        public Profile? Update(Profile profile)
        {
            _dataContext.Profiles.Update(profile);

            _dataContext.SaveChanges();

            return profile;
        }

        public Profile? Get(int id)
        {
            return _dataContext.Profiles.Find(id);
        }

        public Profile? GetByUser(User user)
        {
            return _dataContext.Profiles.FirstOrDefault(p => p.UserId == user.Id);
        }
    };
}
