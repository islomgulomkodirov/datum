﻿using Datum.Models;

namespace Datum.Repositories.CommentRepo
{
    public interface ICommentRepository
    {
        Comment? Create(Comment comment);

        Comment? Update(Comment comment);

        Comment? Get(int id);

        void Delete(Comment comment);

        List<Comment>? GetCommentsByPostId (int postId);

        List<Comment>? GetCommentsByUserId(string userId);


    }
}
