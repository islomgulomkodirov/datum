﻿using Datum.Data;
using Datum.Models;

namespace Datum.Repositories.CommentRepo
{
    public class CommentRepository : ICommentRepository
    {
        private readonly DataContext _dataContext;

        public CommentRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public Comment? Create(Comment comment)
        {
            _dataContext.Comments.Add(comment);
            _dataContext.SaveChanges();

            return comment;
        }

        public Comment? Update(Comment comment)
        {
            _dataContext.Comments.Update(comment);
            _dataContext.SaveChanges();

            return comment;
        }

        public Comment? Get(int id)
        {
            return _dataContext.Comments.Find(id);
        }

        public void Delete(Comment comment)
        {
            _dataContext.Comments.Remove(comment);
            _dataContext.SaveChanges();
        }

        public List<Comment> GetCommentsByPostId(int postId)
        {
            return _dataContext.Comments.Where(c => c.PostId == postId).ToList();
        }
        public List<Comment> GetCommentsByUserId(string userId)
        {
            return _dataContext.Comments.Where(c => c.UserId == userId).ToList();
        }
    };
}
