﻿using Datum.Models;

namespace Datum.Repositories.UserRepo
{
    public interface IUserRepository
    {
        void Create(User user);

        User? Update(User user);

        User? Get(string id);

        void Delete(User user);

        List<User>? GetAll();

        bool CheckUserExistsByUsername(string name);

        bool CheckUserHasProfile(User user);

        string? HashPassword(string password);
    }
}
