﻿using Datum.Data;
using Datum.Models;
using BCrypt.Net;

namespace Datum.Repositories.UserRepo
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _dataContext;

        public UserRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Create(User user)
        {
            _dataContext.Users.Add(user);

            _dataContext.SaveChanges();
        }

        public User? Update(User user)
        {
            _dataContext.Users.Update(user);

            _dataContext.SaveChanges();

            return user;
        }

        public User? Get(string id)
        {
            return _dataContext.Users.Find(id);
        }

        public void Delete(User user)
        {
            _dataContext.Users.Remove(user);

            _dataContext.SaveChanges();
        }

        public List<User> GetAll()
        {
            return _dataContext.Users.OrderBy(u => u.Id).ToList();
        }

        public bool CheckUserExistsByUsername(string name)
        {
            return _dataContext.Users.Any(u => u.UserName == name);
        }

        public bool CheckUserHasProfile(User user)
        {
            return user.Profile != null;
        }

        public string? HashPassword(string password)
        {

            var salt = BCrypt.Net.BCrypt.GenerateSalt();
            var hash = BCrypt.Net.BCrypt.HashPassword(password, salt);

            return hash;

        }
    }
}
