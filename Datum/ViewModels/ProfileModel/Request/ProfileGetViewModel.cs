﻿using System.ComponentModel.DataAnnotations;

namespace Datum.ViewModels.ProfileModel.Request
{
    public class ProfileGetViewModel
    {
        [Required(ErrorMessage = "Id is required")]
        public int? Id { get; set; }
    }
}
