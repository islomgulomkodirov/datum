﻿namespace Datum.ViewModels.ProfileModel.Request
{
    public class ProfileUpdateViewModel
    {
        public string? FullName { get; set; }

        public string? Bio { get; set; }

        public string? AvatarUrl { get; set; }

    }
}
