﻿using Datum.Models;

namespace Datum.ViewModels.ProfileModel.Response
{
    public class ProfileGetResponseViewModel
    {
        public int Id { get; set; }

        public string? FullName { get; set; }

        public string? Bio { get; set;}

        public string? AvatarUrl { get; set; }

        public User? User { get; set; }
    }
}
