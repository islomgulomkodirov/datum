﻿using Datum.Models;

namespace Datum.ViewModels.ProfileModel.Response
{
    public class ProfileResponseViewModel
    {
        public int Id { get; init; }

        public string? FullName { get; set; }

        public string? Bio { get; set; }

        public string? AvatarUrl { get; set; }
    }
}
