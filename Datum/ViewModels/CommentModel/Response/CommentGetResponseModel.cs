﻿using Datum.Models;

namespace Datum.ViewModels.CommentModel.Response
{
    public class CommentGetResponseModel
    {
        public int Id { get; set; }

        public int PostId { get; set; }

        public string? UserId { get; set; }

        public string? Title { get; set; }

        public string? Text { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
