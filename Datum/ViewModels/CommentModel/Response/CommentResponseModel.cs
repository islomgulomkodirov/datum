﻿using Datum.Models;

namespace Datum.ViewModels.CommentModel.Response
{
    public class CommentResponseModel
    {
        public int Id { get; init; }

        public DateTime CreatedAt { get; set; }
    }
}
