﻿using System.ComponentModel.DataAnnotations;
using Datum.Models;

namespace Datum.ViewModels.CommentModel.Request
{
    public class CommentCreateViewModel
    {
        [Required]
        public int PostId { get; set; }

        [Required]
        public string? UserId { get; set; }

        [Required]
        public string? Title { get; set; }

        public string? Text { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}