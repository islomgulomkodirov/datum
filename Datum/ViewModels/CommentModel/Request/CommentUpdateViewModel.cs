﻿using System.ComponentModel.DataAnnotations;
using Datum.Models;

namespace Datum.ViewModels.CommentModel.Request
{
    public class CommentUpdateViewModel
    {
        public string? Title { get; set; }

        public string? Text { get; set; }

    }
}