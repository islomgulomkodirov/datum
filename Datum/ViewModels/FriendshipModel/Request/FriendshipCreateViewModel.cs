﻿using System.ComponentModel.DataAnnotations;
using Datum.Models;

namespace Datum.ViewModels.FriendshipModel.Request
{
    public class FriendshipCreateViewModel
    {
        [Required(ErrorMessage = "User1 Id is required")]
        public string? User1Id { get; set; }

        [Required(ErrorMessage = "User2 Id is required")]
        public string? User2Id { get; set; }
    }
}