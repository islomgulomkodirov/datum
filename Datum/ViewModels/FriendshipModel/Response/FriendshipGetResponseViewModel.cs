﻿using Datum.Models;
using Datum.ViewModels.UserModel.Response;

namespace Datum.ViewModels.FriendshipModel.Response
{
    public class FriendshipGetResponseViewModel
    {
        public int Id { get; set; }

        public bool IsAccepted { get; set; }

        public ICollection<UserResponseViewModel> Users { get; set; } = new List<UserResponseViewModel>();

        public DateTime CreatedAt { get; set; }
    }
}
