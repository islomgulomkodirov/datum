﻿using Datum.Models;

namespace Datum.ViewModels.FriendshipModel.Response
{
    public class FriendshipCreateResponseViewModel
    {
        public int Id { get; set; }

    }
}
