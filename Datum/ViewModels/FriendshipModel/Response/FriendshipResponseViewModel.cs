﻿using Datum.Models;

namespace Datum.ViewModels.FriendshipModel.Response
{
    public class FriendshipResponseViewModel
    {
        public int Id { get; init; }

        public bool IsAccepted { get; set; } = false;
    }
}
