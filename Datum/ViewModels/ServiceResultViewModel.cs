﻿namespace Datum.ViewModels
{
    public class ServiceResultViewModel<T>
    {
        public bool IsSuccessful { get; set; }

        public string? ErrorMessage { get; set; }

        public T? Payload { get; set; }
    }
}
