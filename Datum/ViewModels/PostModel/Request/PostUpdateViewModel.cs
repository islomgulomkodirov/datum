﻿using System.ComponentModel.DataAnnotations;
using Datum.Models;

namespace Datum.ViewModels.PostModel.Request
{
    public class PostUpdateViewModel
    {
        public string? Name { get; set; }

        public string? Description { get; set; }

    }
}