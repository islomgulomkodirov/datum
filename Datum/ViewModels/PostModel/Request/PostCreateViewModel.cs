﻿using System.ComponentModel.DataAnnotations;
using Datum.Models;

namespace Datum.ViewModels.PostModel.Request
{
    public class PostCreateViewModel
    {
        [Required]
        public string? Name { get; set; }

        [Required]
        public string? Description { get; set; }

        [Required] public string? UserId { get; set; }
    }
}