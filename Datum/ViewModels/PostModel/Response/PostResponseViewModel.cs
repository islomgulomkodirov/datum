﻿using Datum.Models;

namespace Datum.ViewModels.PostModel.Response
{
    public class PostResponseViewModel
    {
        public int Id { get; init; }

        public DateTime CreatedAt { get; set; }
    }
}
