﻿using Datum.Models;
using Datum.ViewModels.UserModel.Response;

namespace Datum.ViewModels.PostModel.Response
{
    public class PostGetResponseViewModel
    {
        public int Id { get; set; }

        public string? Name { get; set; }

        public string? Description { get; set; }

        public User? User { get; set; }

        public List<Comment>? Comments { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
