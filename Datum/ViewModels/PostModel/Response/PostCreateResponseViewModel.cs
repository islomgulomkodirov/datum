﻿using Datum.Models;

namespace Datum.ViewModels.PostModel.Response
{
    public class PostCreateResponseViewModel
    {
        public int Id { get; set; }

        public string? UserId { get; set; }

        public string? Name { get; set; }

    }
}
