﻿using System.ComponentModel.DataAnnotations;
using Microsoft.CodeAnalysis.Host.Mef;

namespace Datum.ViewModels.UserModel.Request
{
    public class UserUpdateViewModel
    {
        public string? UserName { get; set; }

        public string? UserEmail { get; set; }

        public string? Password { get; set; }

    }
}
