﻿using Datum.Models;
using Datum.ViewModels.ProfileModel.Response;

namespace Datum.ViewModels.UserModel.Response
{
    public class UserGetResponseViewModel
    {
        public string? Id { get; set; }

        public string? UserName { get; set; }

        public string? UserEmail { get; set; }

        public ProfileGetResponseViewModel? Profile { get; set; }

        public List<Friendship>? Friends { get; set; }

        public List<Post>? Posts { get; set; }

        public List<Comment>? Comments { get; set; }
    }
}
