﻿using Datum.Models;

namespace Datum.ViewModels.UserModel.Response
{
    public class UserCreateResponseViewModel
    {
        public string? Id { get; set; }

        public string? UserName { get; set; }

        public string? UserEmail { get; set;}

        public int ProfileId { get; set;} 
    }
}
