﻿using Datum.Models;

namespace Datum.ViewModels.UserModel.Response
{
    public class UserResponseViewModel
    {
        public string? Id { get; init; }
        public string? UserName { get; set; }
        public string? UserEmail { get; set;}

    }
}
